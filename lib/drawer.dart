import 'package:cashbus_pages/pages/profile%20pages/cash_page.dart';
import 'package:cashbus_pages/pages/profile%20pages/fale_conosco.dart';
import 'package:cashbus_pages/pages/profile%20pages/parceiros.dart';
import 'package:cashbus_pages/pages/profile%20pages/profile_data.dart';
import 'package:cashbus_pages/pages/profile%20pages/qr_code.dart';
import 'package:cashbus_pages/pages/profile%20pages/regulamento_faq.dart';
import 'package:flutter/material.dart';

import 'pages/profile pages/avisos.dart';

class CustomDrawer extends StatefulWidget {
  const CustomDrawer({Key? key}) : super(key: key);

  @override
  State<CustomDrawer> createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Colors.black,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.all(20),
              height: 100,
              width: double.infinity,
              color: Colors.blueGrey[900],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  IconButton(
                    color: Colors.grey,
                    iconSize: 60,
                    icon: const Icon(Icons.menu),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.all(10),
                color: Colors.blueGrey[600],
                height: MediaQuery.of(context).size.height,
                width: double.infinity,
                child: ListView.builder(
                    itemCount: listDrawerSectionNames.length,
                    itemBuilder: (BuildContext context, index) {
                      return buildMenuItem(
                        icon: listIconsDrawer[index],
                        text: listDrawerSectionNames[index],
                        page: listLinksDrawer[index],
                      );
                    }),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildMenuItem({
    required String text,
    required IconData icon,
    required Widget page,
  }) {
    final color = Colors.white;
    final hoverColor = Colors.white70;

    return ListTile(
      contentPadding: const EdgeInsets.only(bottom: 10),
      leading: Icon(icon, color: color, size: 40),
      title: Text(text, style: TextStyle(color: color, fontSize: 25)),
      hoverColor: hoverColor,
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (BuildContext context) => page));
      },
    );
  }
}

enum DrawerSections {
  perfil,
  cash,
  avisos,
  parceiros,
  regulamentoFAQ,
  faleConosco,
  qrCode,
}

List listDrawerSectionNames = [
  "Perfil",
  "Cash",
  "Avisos",
  "Parceiros",
  "Regulamentos | FAQ",
  "Fale Conosco",
  "QR Code"
];

List listIconsDrawer = [
  Icons.person,
  Icons.money_off,
  Icons.warning,
  Icons.handyman_sharp,
  Icons.report,
  Icons.chat,
  Icons.qr_code,
];

List listLinksDrawer = [
  const DadosPerfilPage(),
  const CashPage(),
  const AvisosPage(),
  const ParceirosPage(),
  const RegulamentoFAQPage(),
  const FaleConoscoPage(),
  const QRCodePage(),
];
