import 'package:cashbus_pages/constants.dart';
import 'package:cashbus_pages/drawer.dart';
import 'package:cashbus_pages/pages/create%20account%20pages/create_account_page.dart';
import 'package:cashbus_pages/pages/create%20account%20pages/define_pass_page.dart';
import 'package:cashbus_pages/pages/forgot%20password/forgot_pass_page_1.dart';
import 'package:cashbus_pages/pages/forgot%20password/forgot_pass_page_2.dart';
import 'package:cashbus_pages/pages/forgot%20password/forgot_pass_page_3.dart';
import 'package:cashbus_pages/pages/initial%20pages/login_page.dart';
import 'package:cashbus_pages/pages/initial%20pages/splash.dart';
import 'package:cashbus_pages/pages/profile%20pages/profile_data_edit.dart';
import 'package:cashbus_pages/pages/profile%20pages/qr_code.dart';
//import 'package:cashbus_pages/pages/splash.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'pages/create account pages/code_page.dart';
import 'pages/profile pages/profile_data.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(primary: Colors.black54),
        ),
      ),
      home: const Scaffold(
        body: Splash(),
      ),
    );
  }
}
