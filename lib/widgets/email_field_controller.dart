import 'package:flutter/material.dart';

import '../constants.dart';

class EmailField extends StatefulWidget {
  const EmailField({Key? key}) : super(key: key);

  @override
  _EmailFieldState createState() => _EmailFieldState();
}

class _EmailFieldState extends State<EmailField> {
  String? email;

  TextEditingController emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return buildEmail(emailController);
  }

  Widget buildEmail(emailController) => TextFormField(
      onChanged: (value) => setState(() => email = value),
      //onSubmitted: (value) => setState(() => email = value),
      controller: emailController,
      decoration: InputDecoration(
        hintStyle: const TextStyle(fontSize: 20, color: Colors.grey),
        errorStyle: const TextStyle(color: Colors.white, fontSize: 16),
        filled: true,
        fillColor: Colors.transparent,
        hintText: 'name@example.com',
        //labelText: 'Email',
        //errorText: 'Incorrect email',
        //prefixIcon: const Icon(Icons.mail),
        suffixIcon: emailIsEmptyCondition(emailController),
        border: roundedBorder(),
      ),
      keyboardType: TextInputType.emailAddress,
      textInputAction: TextInputAction.next);

  Widget emailIsEmptyCondition(emailController) => emailController.text.isEmpty
      ? Container(width: 0)
      : IconButton(
          icon: const Icon(Icons.close),
          onPressed: () => emailController.clear(),
        );
}
