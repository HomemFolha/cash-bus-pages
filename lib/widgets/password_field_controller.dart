import 'package:cashbus_pages/constants.dart';
import 'package:flutter/material.dart';

class PasswordField extends StatefulWidget {
  const PasswordField({Key? key}) : super(key: key);

  @override
  _PasswordFieldState createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  String? password;
  TextEditingController passwordController = TextEditingController();
  bool isPasswordVisible = false;

  @override
  Widget build(BuildContext context) => buildPassword(passwordController);

  Widget buildPassword(passwordController) {
    return TextField(
        onChanged: (value) => setState(() => password = value),
        onSubmitted: (value) => setState(() => password = value),
        controller: passwordController,
        decoration: InputDecoration(
          hintStyle: const TextStyle(fontSize: 20, color: Colors.grey),
          errorStyle: const TextStyle(
            color: Colors.white,
            fontSize: 16,
          ),
          filled: true,
          fillColor: Colors.transparent,
          //errorText: 'Incorrect password',
          hintText: 'Your password...',
          //suffixIcon: passwordVisibleIcon(isPasswordVisible),
          border: roundedBorder(),
        ),
        obscureText: true /*!isPasswordVisible*/,
        keyboardType: TextInputType.visiblePassword,
        textInputAction: TextInputAction.next);
  }

  // passwordVisibleIcon(isPasswordVisile) {
  //   IconButton(
  //     icon: isPasswordVisible
  //         ? const Icon(Icons.visibility_off)
  //         : const Icon(Icons.visibility),
  //     onPressed: () => setState(() {
  //       isPasswordVisible = !isPasswordVisible;
  //     }),
  //   );
  // }
}
