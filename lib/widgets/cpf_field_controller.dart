import 'package:cashbus_pages/constants.dart';
import 'package:flutter/material.dart';

class CpfField extends StatefulWidget {
  const CpfField({Key? key}) : super(key: key);

  @override
  _CpfFieldState createState() => _CpfFieldState();
}

class _CpfFieldState extends State<CpfField> {
  String? cpf;

  TextEditingController cpfController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return buildCPF(cpfController);
  }

  Widget buildCPF(cpfController) => TextFormField(
      onChanged: (value) => setState(() => cpf = value),
      //onSubmitted: (value) => setState(() => email = value),
      controller: cpfController,
      decoration: InputDecoration(
        counter: const Offstage(),
        hintStyle: const TextStyle(fontSize: 20, color: Colors.grey),
        errorStyle: const TextStyle(color: Colors.white, fontSize: 16),
        filled: true,
        fillColor: Colors.transparent,
        hintText: '000.000.000-00',
        border: roundedBorder(),
      ),
      maxLength: 11,
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.next);
}
