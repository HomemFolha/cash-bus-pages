import 'package:cashbus_pages/constants.dart';
import 'package:cashbus_pages/pages/initial%20pages/login_page.dart';
import 'package:flutter/material.dart';

class SignUpPage extends StatelessWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
      body: Container(
        decoration: gradientBackground,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.of(context).pop(null),
        child: const Icon(
          Icons.arrow_back_ios,
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
      ),
    );
  }
}
