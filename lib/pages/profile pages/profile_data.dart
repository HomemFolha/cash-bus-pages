import 'package:cashbus_pages/drawer.dart';
import 'package:cashbus_pages/pages/profile%20pages/profile_data_edit.dart';
import 'package:flutter/material.dart';

class DadosPerfilPage extends StatefulWidget {
  const DadosPerfilPage({Key? key}) : super(key: key);

  @override
  State<DadosPerfilPage> createState() => _DadosPerfilPageState();
}

class _DadosPerfilPageState extends State<DadosPerfilPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const CustomDrawer(),
      body: Container(
        padding:
            const EdgeInsets.only(top: 50, left: 40, right: 40, bottom: 20),
        width: double.infinity,
        height: double.infinity,
        color: Colors.blueGrey[900],
        child: userData(),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => const DadosPerfilEditarPage()));
        },
        child: const Icon(Icons.edit),
        backgroundColor: Colors.grey,
      ),
    );
  }

  Widget userData() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        containerInfo("Nome", "João da Silva"),
        containerInfo("RG", "698500142"),
        containerInfo("CPF", "021345987-43"),
        containerInfo("Nome", "João da Silva"),
        containerInfo("E-mail", "example@example.com"),
        containerInfo("Fone", "9999999999"),
        containerInfo("Endereço", "Rua A, 250"),
        containerInfo("Cidade", "Manaus"),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
              flex: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  containerInfo("Estado", "AM"),
                ],
              ),
            ),
            Expanded(
              flex: 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  containerInfo("CEP", "94455-290"),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget containerInfo(textType, textInfo) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(textType,
            style: const TextStyle(fontSize: 25, color: Colors.white)),
        const SizedBox(height: 1),
        Text(textInfo,
            style: const TextStyle(
                fontSize: 27,
                color: Colors.white,
                fontWeight: FontWeight.bold)),
        const SizedBox(height: 5),
      ],
    );
  }
}
