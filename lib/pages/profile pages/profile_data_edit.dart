import 'package:cashbus_pages/constants.dart';
import 'package:cashbus_pages/drawer.dart';
import 'package:flutter/material.dart';

class DadosPerfilEditarPage extends StatefulWidget {
  const DadosPerfilEditarPage({Key? key}) : super(key: key);

  @override
  _DadosPerfilEditarPageState createState() => _DadosPerfilEditarPageState();
}

class _DadosPerfilEditarPageState extends State<DadosPerfilEditarPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const CustomDrawer(),
      body: Container(
        padding:
            const EdgeInsets.only(top: 40, left: 30, right: 30, bottom: 20),
        color: Colors.blueGrey[900],
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Icon(Icons.person, size: 200, color: Colors.white),
              const SizedBox(height: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Icon(Icons.edit, size: 30, color: white),
                  SizedBox(width: 10),
                  Text('Editar', style: TextStyle(fontSize: 30, color: white))
                ],
              ),
              containerEditInfo("Nome", "João da Silva"),
              containerEditInfo("RG", "698500142"),
              containerEditInfo("CPF", "021345987-43"),
              containerEditInfo("Nome", "João da Silva"),
              containerEditInfo("E-mail", "example@example.com"),
              containerEditInfo("Fone", "9999999999"),
              containerEditInfo("Endereço", "Rua A, 250"),
              containerEditInfo("Cidade", "Manaus"),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Expanded(
                    flex: 2,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        containerEditInfo("Estado", "AM"),
                      ],
                    ),
                  ),
                  const SizedBox(width: 30),
                  Expanded(
                    flex: 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        containerEditInfo("CEP", "94455-290"),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget containerEditInfo(textType, hintText) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(textType,
            style: const TextStyle(fontSize: 25, color: Colors.white)),
        const SizedBox(height: 1),
        ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: TextFormField(
            decoration: InputDecoration(
              hintText: hintText,
              fillColor: Colors.white,
              filled: true,
            ),
          ),
        ),
        const SizedBox(height: 5),
      ],
    );
  }
}
