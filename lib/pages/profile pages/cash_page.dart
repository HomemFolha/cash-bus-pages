import 'dart:convert';

import 'package:cashbus_pages/drawer.dart';
import 'package:flutter/material.dart';

class CashPage extends StatelessWidget {
  const CashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const CustomDrawer(),
      body: Container(
        color: Colors.grey[900],
        padding: const EdgeInsets.all(30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: MediaQuery.of(context).size.width * 0.9,
              height: MediaQuery.of(context).size.height * 0.30,
              color: Colors.transparent,
              child: Image.asset('assets/images/cashbus_logo_onlywhite.png'),
            ),
            const SizedBox(height: 30),
            SingleChildScrollView(
              child: Column(
                children: const <Widget>[
                  // for (var i = 0; i < listSpendings.length; i++)
                  //   spendings(listSpendings["text"][i], listSpendings["price"][i],
                  //       listSpendings["isGasto"][i]);
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  spendings(String text, String price, bool isGasto) {
    return Text(
      text + " ......... " + price,
      style: TextStyle(color: isGasto ? Colors.green : Colors.red),
    );
  }
}

final listSpendings = [
  {
    "text": "Oi",
    "price": "389.90",
    "isGasto": true,
  },
  {
    "text": "Oi",
    "price": "232.90",
    "isGasto": false,
  },
  {
    "text": "Oi",
    "price": "432.90",
    "isGasto": true,
  },
  {
    "text": "Oi",
    "price": "7657.90",
    "isGasto": false,
  },
];
