import 'package:cashbus_pages/drawer.dart';
import 'package:flutter/material.dart';

class QRCodePage extends StatelessWidget {
  const QRCodePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const CustomDrawer(),
      body: Container(
        color: Colors.blueGrey[900],
        child: const Center(
          child: Icon(Icons.qr_code, size: 200, color: Colors.white),
        ),
      ),
    );
  }
}
