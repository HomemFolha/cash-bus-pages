import 'package:cashbus_pages/drawer.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';

class AvisosPage extends StatefulWidget {
  const AvisosPage({Key? key}) : super(key: key);

  @override
  State<AvisosPage> createState() => _AvisosPageState();
}

class _AvisosPageState extends State<AvisosPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const CustomDrawer(),
      body: Container(
        width: double.infinity,
        color: Colors.grey[900],
        padding: const EdgeInsets.all(40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Icon(
              Icons.notifications,
              size: 150,
              color: Colors.white,
            ),
            const SizedBox(height: 15),
            for (var i = 0; i < dates.length; i++)
              Column(
                children: [
                  buildMessage(dates[i], texts[i]),
                  const SizedBox(height: 20),
                ],
              ),
          ],
        ),
      ),
    );
  }

  buildMessage(date, text) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(date,
            style: const TextStyle(
              fontSize: 20,
              color: white,
            )),
        const SizedBox(height: 10),
        Align(
          alignment: Alignment.centerRight,
          child: message(text),
        ),
      ],
    );
  }

  Widget message(text) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(15),
      child: Container(
        //width: MediaQuery.of(context).size.width * 0.6,
        decoration: gradientBackground,
        padding: const EdgeInsets.all(20),
        child: Text(text,
            style: const TextStyle(
              fontSize: 16,
              color: white,
            )),
      ),
    );
  }
}

List dates = ["Segunda - 23/08", "Quarta - 25/08"];
List texts = ["Lorem ipum na veia", "Lorem ipum é vida"];
