import 'package:cashbus_pages/drawer.dart';
import 'package:cashbus_pages/pages/profile%20pages/qr_code.dart';
import 'package:flutter/material.dart';

class BaseProfilePage extends StatefulWidget {
  const BaseProfilePage({Key? key}) : super(key: key);

  @override
  _BaseProfilePageState createState() => _BaseProfilePageState();
}

class _BaseProfilePageState extends State<BaseProfilePage> {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        drawer: CustomDrawer(),
        body: QRCodePage(),
      ),
    );
  }
}
