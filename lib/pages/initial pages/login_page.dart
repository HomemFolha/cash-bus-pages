import 'package:cashbus_pages/constants.dart';
import 'package:cashbus_pages/pages/create%20account%20pages/create_account_page.dart';
import 'package:cashbus_pages/pages/forgot%20password/forgot_pass_page_1.dart';
import 'package:cashbus_pages/pages/profile%20pages/base_profile.dart';
import 'package:cashbus_pages/pages/profile%20pages/qr_code.dart';
import 'package:cashbus_pages/pages/signup_page.dart';
import 'package:flutter/material.dart';
//import 'package:get/get.dart';
import 'package:cashbus_pages/widgets/email_field_controller.dart';
import 'package:cashbus_pages/widgets/password_field_controller.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String? username;
  String? email;
  String? password;

  TextEditingController usernameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          decoration: gradientBackground,
          child: Padding(
            padding:
                const EdgeInsets.only(left: 35, right: 35, bottom: 10, top: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  height: MediaQuery.of(context).size.height * 0.30,
                  color: Colors.transparent,
                  child:
                      Image.asset('assets/images/cashbus_logo_onlywhite.png'),
                ),
                const SizedBox(height: 20),
                const Text(
                  'Nome',
                  style: TextStyle(
                    fontSize: 25.0,
                    color: Colors.white,
                  ),
                ),
                const SizedBox(height: 10),
                buildUsername(usernameController),
                const SizedBox(height: 10),
                const Text(
                  'E-mail',
                  style: TextStyle(
                    fontSize: 25.0,
                    color: Colors.white,
                  ),
                ),
                const SizedBox(height: 10),
                const EmailField(),
                const SizedBox(height: 20),
                const Text(
                  'Password',
                  style: TextStyle(
                    fontSize: 25.0,
                    color: Colors.white,
                  ),
                ),
                const SizedBox(height: 10),
                const PasswordField(),
                const SizedBox(height: 30),
                buildSubmit(),
                SizedBox(
                  width: double.infinity,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      buildTextButton(
                          const ForgotPassPage1(), 'Esqueci minha senha'),
                      buildTextButton(const WelcomePage(), 'Criar uma conta'),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  // EMAIL FORM FIELD AND LOGIC
  Widget buildUsername(usernameController) => TextFormField(
      onChanged: (value) => setState(() => username = value),
      //onSubmitted: (value) => setState(() => email = value),
      controller: usernameController,
      decoration: InputDecoration(
        hintStyle: const TextStyle(fontSize: 20, color: Colors.grey),
        errorStyle: const TextStyle(color: Colors.white, fontSize: 16),
        filled: true,
        fillColor: Colors.transparent,
        hintText: 'Username',
        border: roundedBorder(),
      ),
      textInputAction: TextInputAction.next);

// PASSWORD FORM FIELD AND LOGIC

  Text loginText(String textField) {
    return const Text(
      'textField',
      style: TextStyle(
        fontSize: 22.0,
        color: Colors.white,
      ),
    );
  }

  Widget buildSubmit() {
    return ConstrainedBox(
      constraints:
          const BoxConstraints.tightFor(width: double.infinity, height: 50),
      child: ElevatedButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const BaseProfilePage()));
        },
        style: buttonStyle(),
        child: const Text(
          'Acessar',
          style: TextStyle(fontSize: 30),
        ),
      ),
    );
  }

  ButtonStyle buttonStyle() {
    return ButtonStyle(
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(18.0),
      //side: BorderSide(color: Colors.red),
    )));
  }

  TextButton buildTextButton(Widget function, String text) {
    return TextButton(
      style: const ButtonStyle(),
      onPressed: () => Navigator.push(context,
          MaterialPageRoute(builder: (BuildContext context) => function)),
      child:
          Text(text, style: const TextStyle(fontSize: 15, color: Colors.white)),
    );
  }

  void signUpPage() {
    Navigator.pushReplacement<void, void>(
      context,
      MaterialPageRoute<void>(
        builder: (BuildContext context) => const SignUpPage(),
      ),
    );
  }

  void forgotPassPage() {
    Navigator.pushReplacement<void, void>(
      context,
      MaterialPageRoute<void>(
        builder: (BuildContext context) => const ForgotPassPage1(),
      ),
    );
  }
}
