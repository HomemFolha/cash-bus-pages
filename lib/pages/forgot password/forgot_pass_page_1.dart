import 'package:cashbus_pages/pages/forgot%20password/forgot_pass_page_2.dart';
import 'package:cashbus_pages/widgets/cpf_field_controller.dart';
import 'package:cashbus_pages/widgets/email_field_controller.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';

class ForgotPassPage1 extends StatefulWidget {
  const ForgotPassPage1({Key? key}) : super(key: key);

  @override
  State<ForgotPassPage1> createState() => _ForgotPassPage1State();
}

class _ForgotPassPage1State extends State<ForgotPassPage1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: gradientBackground,
        child: Padding(
          padding:
              const EdgeInsets.only(left: 45, right: 45, bottom: 10, top: 90),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            // ignore: prefer_const_literals_to_create_immutables
            children: [
              const Text(
                'Esqueci minha \nsenha',
                style: TextStyle(
                    fontSize: 35, fontWeight: FontWeight.bold, color: white),
              ),
              const SizedBox(height: 10),
              const Text(
                'Enviaremos um PIN para seu e-mail, para você trocar sua senha.',
                style: TextStyle(
                  fontSize: 24,
                  color: white,
                ),
              ),
              const SizedBox(height: 50),
              const Text(
                'CPF',
                style: TextStyle(
                  fontSize: 25.0,
                  color: Colors.white,
                ),
              ),
              const SizedBox(height: 10),
              const CpfField(),
              const SizedBox(height: 20),
              const Text(
                'E-mail',
                style: TextStyle(
                  fontSize: 25.0,
                  color: Colors.white,
                ),
              ),
              const SizedBox(height: 10),
              const EmailField(),
              const SizedBox(height: 40),
              sendToEmailButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget sendToEmailButton() {
    return ConstrainedBox(
      constraints:
          const BoxConstraints.tightFor(width: double.infinity, height: 50),
      child: ElevatedButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => const ForgotPassPage2()));
        },
        style: buttonStyle(),
        child: const Text(
          'Enviar Para E-mail',
          style: TextStyle(fontSize: 30),
        ),
      ),
    );
  }
}
