import 'package:cashbus_pages/constants.dart';
import 'package:cashbus_pages/widgets/password_field_controller.dart';
import 'package:flutter/material.dart';

class ForgotPassPage3 extends StatefulWidget {
  const ForgotPassPage3({Key? key}) : super(key: key);

  @override
  _ForgotPassPage3State createState() => _ForgotPassPage3State();
}

class _ForgotPassPage3State extends State<ForgotPassPage3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: gradientBackground,
        child: Padding(
          padding:
              const EdgeInsets.only(left: 35, right: 35, bottom: 10, top: 50),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Esqueci minha \nsenha',
                style: TextStyle(
                    fontSize: 35, fontWeight: FontWeight.bold, color: white),
              ),
              const SizedBox(height: 10),
              const Text(
                'Defina sua nova senha',
                style: TextStyle(
                  fontSize: 24,
                  color: white,
                ),
              ),
              const SizedBox(height: 60),
              const Text(
                'Senha',
                style: TextStyle(
                  fontSize: 25.0,
                  color: Colors.white,
                ),
              ),
              const SizedBox(height: 10),
              const PasswordField(),
              const SizedBox(height: 20),
              const Text(
                'Confirmar senha',
                style: TextStyle(
                  fontSize: 25.0,
                  color: Colors.white,
                ),
              ),
              const SizedBox(height: 10),
              const PasswordField(),
              const SizedBox(height: 60),
              changePassButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget changePassButton() {
    return ConstrainedBox(
      constraints:
          const BoxConstraints.tightFor(width: double.infinity, height: 50),
      child: ElevatedButton(
        onPressed: () {},
        style: buttonStyle(),
        child: const Text(
          'Alterar Senha',
          style: TextStyle(fontSize: 30),
        ),
      ),
    );
  }
}
