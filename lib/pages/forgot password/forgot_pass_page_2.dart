import 'package:cashbus_pages/constants.dart';
import 'package:cashbus_pages/pages/forgot%20password/forgot_pass_page_3.dart';
//import 'package:cashbus_pages/page_template.dart';
import 'package:flutter/material.dart';

class ForgotPassPage2 extends StatefulWidget {
  const ForgotPassPage2({Key? key}) : super(key: key);

  @override
  _ForgotPassPage2State createState() => _ForgotPassPage2State();
}

class _ForgotPassPage2State extends State<ForgotPassPage2> {
  String email = 'random@random.com';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: gradientBackground,
        child: Padding(
          padding:
              const EdgeInsets.only(left: 45, right: 45, bottom: 10, top: 90),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Esqueci minha \nsenha',
                style: TextStyle(
                    fontSize: 35, fontWeight: FontWeight.bold, color: white),
              ),
              const SizedBox(height: 10),
              // ignore: unnecessary_const
              const Text(
                'Insira o código enviado para:\n email@email.com',
                style: TextStyle(
                  fontSize: 24,
                  color: white,
                ),
              ),
              const SizedBox(height: 60),
              buildFourDigit(),
              const SizedBox(height: 80),
              continueButton(),
              const SizedBox(height: 15),
              const SizedBox(
                width: double.infinity,
                child: Text(
                  'Reenviar o código',
                  style: TextStyle(
                    fontSize: 24,
                    color: white,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget continueButton() {
    return ConstrainedBox(
      constraints:
          const BoxConstraints.tightFor(width: double.infinity, height: 50),
      child: ElevatedButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => const ForgotPassPage3()));
        },
        style: buttonStyle(),
        child: const Text(
          'Continuar',
          style: TextStyle(fontSize: 30),
        ),
      ),
    );
  }
}
