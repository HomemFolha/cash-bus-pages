import 'package:cashbus_pages/pages/create%20account%20pages/define_pass_page.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';

class CodePage extends StatefulWidget {
  const CodePage({Key? key}) : super(key: key);

  @override
  State<CodePage> createState() => _CodePageState();
}

class _CodePageState extends State<CodePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: gradientBackground,
        child: Padding(
          padding:
              const EdgeInsets.only(left: 35, right: 35, bottom: 10, top: 50),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              RichText(
                text: const TextSpan(
                    text: 'Insira o código enviado para:\n',
                    style: TextStyle(
                      fontSize: 24,
                      color: white,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                          text: 'email@email.com',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 24,
                            color: white,
                          )),
                    ]),
              ),
              const SizedBox(height: 30),
              buildFourDigit(),
              const SizedBox(height: 140),
              continueButton(),
              const SizedBox(height: 5),
              SizedBox(
                width: double.infinity,
                child: TextButton(
                    onPressed: () {},
                    child: const Text('Eu não recebi um código',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: white,
                          fontSize: 25,
                        ))),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget continueButton() {
    return ConstrainedBox(
      constraints:
          const BoxConstraints.tightFor(width: double.infinity, height: 50),
      child: ElevatedButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => const DefinePassPage()));
        },
        style: buttonStyle(),
        child: const Text(
          'Continuar',
          style: TextStyle(fontSize: 30),
        ),
      ),
    );
  }
}
