import 'package:cashbus_pages/constants.dart';
import 'package:cashbus_pages/widgets/password_field_controller.dart';
import 'package:flutter/material.dart';

class DefinePassPage extends StatefulWidget {
  const DefinePassPage({Key? key}) : super(key: key);

  @override
  _DefinePassPageState createState() => _DefinePassPageState();
}

class _DefinePassPageState extends State<DefinePassPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: gradientBackground,
        child: Padding(
          padding:
              const EdgeInsets.only(left: 35, right: 35, bottom: 10, top: 50),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Defina sua senha',
                style: TextStyle(
                    fontSize: 35, fontWeight: FontWeight.bold, color: white),
              ),
              const SizedBox(height: 30),
              const Text(
                'Senha',
                style: TextStyle(
                  fontSize: 25.0,
                  color: Colors.white,
                ),
              ),
              //PASSWORD
              const SizedBox(height: 10),
              const PasswordField(),
              const SizedBox(height: 20),
              const Text(
                'Confirmar Senha',
                style: TextStyle(
                  fontSize: 25.0,
                  color: Colors.white,
                ),
              ),
              //CONFIRME PASSWORD
              const SizedBox(height: 10),
              const PasswordField(),
              const SizedBox(height: 80),
              changePasswordButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget changePasswordButton() {
    return ConstrainedBox(
      constraints:
          const BoxConstraints.tightFor(width: double.infinity, height: 50),
      child: ElevatedButton(
        onPressed: () {},
        style: buttonStyle(),
        child: const Text(
          'Alterar Senha',
          style: TextStyle(fontSize: 30),
        ),
      ),
    );
  }
}
