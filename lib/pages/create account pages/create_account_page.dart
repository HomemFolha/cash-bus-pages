// ignore_for_file: must_be_immutable

import 'package:cashbus_pages/constants.dart';
import 'package:cashbus_pages/pages/create%20account%20pages/code_page.dart';
import 'package:flutter/material.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);

  //String? name, rg, cpf, email, phone, address, city, state, cep;

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  String? name, rg, cpf, email, phone, address, city, state, cep;
  bool isChecked = false;

  TextEditingController nameController = TextEditingController();
  TextEditingController rgController = TextEditingController();
  TextEditingController cpfController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController stateController = TextEditingController();
  TextEditingController cepController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: gradientBackground,
        child: Padding(
          padding:
              const EdgeInsets.only(left: 35, right: 35, bottom: 10, top: 50),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  'Bem Vindo ao\n Cashbus!',
                  style: TextStyle(
                      fontSize: 35, fontWeight: FontWeight.bold, color: white),
                ),
                const SizedBox(height: 10),
                // ignore: unnecessary_const
                const Text(
                  'Crie aqui a sua conta.',
                  style: TextStyle(
                    fontSize: 24,
                    color: white,
                  ),
                ),
                const SizedBox(height: 10),
                buildForm(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildForm() {
    return Form(
      key: _formKey,
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text('Nome',
                style: TextStyle(fontSize: 20, color: Colors.white)),
            const SizedBox(height: 1),
            buildTextFormField(
                name, nameController, 'João da Silva', TextInputType.name, 50),
            const Text('RG',
                style: TextStyle(fontSize: 20, color: Colors.white)),
            const SizedBox(height: 1),
            buildTextFormField(
                rg, rgController, '0000000000', TextInputType.name, 10),
            const Text('CPF',
                style: TextStyle(fontSize: 20, color: Colors.white)),
            const SizedBox(height: 1),
            buildTextFormField(
                cpf, cpfController, '0000000000', TextInputType.number, 11),
            const Text('E-mail',
                style: TextStyle(fontSize: 20, color: Colors.white)),
            const SizedBox(height: 1),
            buildTextFormField(email, emailController, 'example@email.com',
                TextInputType.emailAddress, 40),
            const Text('Fone',
                style: TextStyle(fontSize: 20, color: Colors.white)),
            const SizedBox(height: 1),
            buildTextFormField(phone, phoneController, '(00) 0000 00 000',
                TextInputType.phone, 11),
            const Text('Endereço',
                style: TextStyle(fontSize: 20, color: Colors.white)),
            const SizedBox(height: 1),
            buildTextFormField(address, addressController, 'Rua A, 250',
                TextInputType.name, 50),
            const Text('Cidade',
                style: TextStyle(fontSize: 20, color: Colors.white)),
            const SizedBox(height: 1),
            buildTextFormField(
                city, cityController, 'Manaus', TextInputType.name, 20),
            const Text('Cidade',
                style: TextStyle(fontSize: 20, color: Colors.white)),
            const SizedBox(height: 1),
            buildTextFormField(
                city, cityController, 'Manaus', TextInputType.name, 20),
            Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text('Estado',
                          style: TextStyle(fontSize: 20, color: Colors.white)),
                      const SizedBox(height: 1),
                      buildTextFormField(
                          state, stateController, 'AM', TextInputType.name, 2),
                    ],
                  ),
                ),
                const SizedBox(width: 20),
                Expanded(
                  flex: 3,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text('CEP',
                          style: TextStyle(fontSize: 20, color: Colors.white)),
                      const SizedBox(height: 1),
                      buildTextFormField(cep, cepController, '94455-290',
                          TextInputType.number, 8),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Checkbox(
                    checkColor: Colors.green,
                    value: isChecked,
                    onChanged: (bool? value) {
                      setState(() {
                        isChecked = value!;
                      });
                    }),
                const Text('Aceito os termos e condições',
                    style: TextStyle(fontSize: 20, color: Colors.white)),
              ],
            ),
            const SizedBox(height: 20),
            createAccountButton(),
          ],
        ),
      ),
    );
  }

  Widget buildTextFormField(
      typeField, controller, hintText, keyboardType, int? maxLength) {
    return TextFormField(
      onChanged: (value) => setState(() => typeField = value),
      controller: controller,
      decoration: InputDecoration(
        counter: const Offstage(),
        contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
        hintStyle: const TextStyle(fontSize: 18, color: Colors.grey),
        filled: true,
        fillColor: Colors.white,
        hintText: hintText,
        border: customRoundedBorder(),
      ),
      keyboardType: keyboardType,
      textInputAction: TextInputAction.next,
      maxLength: maxLength ?? 30,
      maxLines: 1,
    );
  }

  OutlineInputBorder customRoundedBorder() {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(15),
      borderSide: BorderSide(
        color: Colors.black.withOpacity(0.5),
        width: 10,
      ),
    );
  }

  Widget createAccountButton() {
    return ConstrainedBox(
      constraints:
          const BoxConstraints.tightFor(width: double.infinity, height: 50),
      child: ElevatedButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => const CodePage()));
        },
        style: buttonStyle(),
        child: const Text(
          'Criar Conta',
          style: TextStyle(fontSize: 30),
        ),
      ),
    );
  }
}
