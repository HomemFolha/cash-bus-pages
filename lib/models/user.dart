class User {
  User({
    required this.name,
    required this.rg,
    required this.cpf,
    required this.email,
    required this.telephone,
    required this.address,
    required this.city,
    required this.state,
    required this.cep,
  });

  String name;
  String rg;
  String cpf;
  String email;
  String telephone;
  String address;
  String city;
  String state;
  String cep;
}
