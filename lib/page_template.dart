import 'package:cashbus_pages/constants.dart';
import 'package:flutter/material.dart';

class Template extends StatelessWidget {
  const Template({Key? key, List<Widget>? children}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: gradientBackground,
      child: Padding(
        padding:
            const EdgeInsets.only(left: 45, right: 45, bottom: 10, top: 90),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
        ),
      ),
    );
  }
}
