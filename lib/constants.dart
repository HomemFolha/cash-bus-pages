import 'package:flutter/material.dart';

// COLORS
const Color firstBackgroundColor = Color(0xFF1A497F);
const Color secondBackgroundColor = Color(0xFF01B868);
const Color white = Colors.white;

// IMAGES
const String imageSplash = 'assets/images/cashbus_splash.webp';
const String logoCashBus = 'assets/images/cashbus_logo_contorno.webp';

// WIDGETS
const BoxDecoration gradientBackground = BoxDecoration(
  gradient: LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [
      firstBackgroundColor,
      secondBackgroundColor,
    ],
  ),
);

Widget pageTemplate(Widget remainingPage) {
  return Container(
    width: double.infinity,
    height: double.infinity,
    decoration: gradientBackground,
    child: Padding(
      padding: const EdgeInsets.only(left: 35, right: 35, bottom: 10, top: 50),
      child: remainingPage,
    ),
  );
}

OutlineInputBorder roundedBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: BorderSide(
      color: Colors.grey.withOpacity(0.5),
      width: 10,
    ),
  );
}

Text customText(String text) {
  return Text(text);
}

ButtonStyle buttonStyle() {
  return ButtonStyle(
      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(18.0),
    //side: BorderSide(color: Colors.red),
  )));
}

Widget buildFourDigit() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceAround,
    children: [
      for (var i = 0; i < 4; i++)
        Container(
          width: 60,
          height: 60,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: white,
          ),
        ),
    ],
  );
}
